/*
 * Copyright (c) Codiad & Andr3as, distributed
 * as-is and without warranty under the MIT License.
 * See http://opensource.org/licenses/MIT for more information. 
 * This information must remain intact.
 */
(function (global, $) {

    !function (a, b) {
        "function" == typeof define && define.amd ? define([], b) : "undefined" != typeof module && module.exports ? module.exports = b() : a.ReconnectingWebSocket = b()
    }(this, function () {
        function a(b, c, d) {
            function l(a, b) {
                var c = document.createEvent("CustomEvent");
                return c.initCustomEvent(a, !1, !1, b), c
            }

            var e = {
                debug: !1,
                automaticOpen: !0,
                reconnectInterval: 1e3,
                maxReconnectInterval: 3e4,
                reconnectDecay: 1.5,
                timeoutInterval: 2e3
            };
            d || (d = {});
            for (var f in e) this[f] = "undefined" != typeof d[f] ? d[f] : e[f];
            this.url = b, this.reconnectAttempts = 0, this.readyState = WebSocket.CONNECTING, this.protocol = null;
            var h, g = this, i = !1, j = !1, k = document.createElement("div");
            k.addEventListener("open", function (a) {
                g.onopen(a)
            }), k.addEventListener("close", function (a) {
                g.onclose(a)
            }), k.addEventListener("connecting", function (a) {
                g.onconnecting(a)
            }), k.addEventListener("message", function (a) {
                g.onmessage(a)
            }), k.addEventListener("error", function (a) {
                g.onerror(a)
            }), this.addEventListener = k.addEventListener.bind(k), this.removeEventListener = k.removeEventListener.bind(k), this.dispatchEvent = k.dispatchEvent.bind(k), this.open = function (b) {
                h = new WebSocket(g.url, c || []), b || k.dispatchEvent(l("connecting")), (g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "attempt-connect", g.url);
                var d = h, e = setTimeout(function () {
                    (g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "connection-timeout", g.url), j = !0, d.close(), j = !1
                }, g.timeoutInterval);
                h.onopen = function () {
                    clearTimeout(e), (g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "onopen", g.url), g.protocol = h.protocol, g.readyState = WebSocket.OPEN, g.reconnectAttempts = 0;
                    var d = l("open");
                    d.isReconnect = b, b = !1, k.dispatchEvent(d)
                }, h.onclose = function (c) {
                    if (clearTimeout(e), h = null, i) g.readyState = WebSocket.CLOSED, k.dispatchEvent(l("close")); else {
                        g.readyState = WebSocket.CONNECTING;
                        var d = l("connecting");
                        d.code = c.code, d.reason = c.reason, d.wasClean = c.wasClean, k.dispatchEvent(d), b || j || ((g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "onclose", g.url), k.dispatchEvent(l("close")));
                        var e = g.reconnectInterval * Math.pow(g.reconnectDecay, g.reconnectAttempts);
                        setTimeout(function () {
                            g.reconnectAttempts++, g.open(!0)
                        }, e > g.maxReconnectInterval ? g.maxReconnectInterval : e)
                    }
                }, h.onmessage = function (b) {
                    (g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "onmessage", g.url, b.data);
                    var c = l("message");
                    c.data = b.data, k.dispatchEvent(c)
                }, h.onerror = function (b) {
                    (g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "onerror", g.url, b), k.dispatchEvent(l("error"))
                }
            }, 1 == this.automaticOpen && this.open(!1), this.send = function (b) {
                if (h) return (g.debug || a.debugAll) && console.debug("ReconnectingWebSocket", "send", g.url, b), h.send(b);
                throw"INVALID_STATE_ERR : Pausing to reconnect websocket"
            }, this.close = function (a, b) {
                "undefined" == typeof a && (a = 1e3), i = !0, h && h.close(a, b)
            }, this.refresh = function () {
                h && h.close()
            }
        }

        return a.prototype.onopen = function () {
        }, a.prototype.onclose = function () {
        }, a.prototype.onconnecting = function () {
        }, a.prototype.onmessage = function () {
        }, a.prototype.onerror = function () {
        }, a.debugAll = !1, a.CONNECTING = WebSocket.CONNECTING, a.OPEN = WebSocket.OPEN, a.CLOSING = WebSocket.CLOSING, a.CLOSED = WebSocket.CLOSED, a
    });

    var WebSocketArgon = (function () {

        var p = {},
            self = {},
            ws = null;

        self.init = function (ip) {
            if (ip === []) {
                return self;
            }
            ws = new ReconnectingWebSocket('ws://' + ip + ':8080');

            self.events();

            self.udevents();

            return self;

        };

        self.instance = function () {
            return ws;
        };

        self.events = function () {

            ws.onopen = function () {
                console.log('Connection open');
            };

            ws.onclose = function () {
                console.log('Connection closed');
            };

            ws.onerror = function (evt) {
                console.log('Connection error: ' + event.data);
            };

            ws.onmessage = function (event) {
                if (event.data === 'refreshFileManager') {

                    setTimeout(function () {
                        console.log('========================================test refresh========================================');
                        /*codiad.filemanager.index(codiad.project.getCurrent(), true);*/
                        codiad.filemanager.rescan(codiad.project.getCurrent());
                    }, 2000);

                    return false;
                }

                $('#argon-terminal-output').append('<span style="display: block">' + event.data + '</span>');
                $('#argon-terminal-output').scrollTop = $('#argon-terminal-output').height();
                $('#argon-terminal').animate({scrollTop: $('#argon-terminal').prop("scrollHeight") - $('#argon-terminal').height()}, 50);
                console.log('Message: ' + event.data);

            };

        };

        self.refresh = function () {
            ws.refresh();
        };

        self.udevents = function () {

            $('#1').on('click', function (e) {
                ws.send("echo 'JoeWalnes was here.'");
            });

            $('#2').on('click', function (e) {
                ws.send('echo');
            });

            $('#3').on('click', function (e) {
                ws.send("count 20");
            });

            $('#4').on('click', function (e) {
                ws.send("count 'Joe'");
            });

            $('#5').on('click', function (e) {
                ws.send("ping www.google.com");
            });

            $('#6').on('click', function (e) {
                ws.send("pong python");
            });

            $('#argon-compile').on('click', function (e) {
                codiad.filemanager.init();
                var fullPath = '/var/www/html/workspace/' + codiad.project.getCurrent();
                ws.send("compile " + fullPath);
            });

            $('#argon-deploy').on('click', function (e) {
                codiad.filemanager.init();
                var fullPath = '/var/www/html/workspace/' + codiad.project.getCurrent() + '/BUILD/' + codiad.project.getCurrent() + '.bin';
                ws.send("deploy " + fullPath);
            });

            $('#7').on('click', function (e) {
                ws.refresh();
            });

        };

        self.callScript = function ($scriptName) {

            $('#argon-output').append('<hr>');

            if (!$('#argon-output').hasClass('opened')) {
                $('#argon-output').toggleClass('opened', 'closed');
                $('#argon-terminal').slideToggle({
                    direction: "up",
                }, 300).css('overflow-y', 'scroll');
            }

            return ws.send($scriptName);
        };

        return self;

    })();

    //Jquery extensions
    $.fn.extend({
        startLoading: function () {
            $(this).attr('disabled', true);
            $(this).find('span').hide();
            $(this).append('<span class="spinner"></span>');
            return $(this);
        },
        stopLoading: function () {
            $(this).find('span').show();
            $(this).find('.spinner').remove();
            $(this).attr('disabled', false);
            return $(this);
        },
        hasLoading: function () {
            return $(this).find('.spinner').length > 0;
        },
    });

    $.QueryString = (function (paramsArray) {

        var params = {};

        for (var i = 0; i < paramsArray.length; ++i) {
            var param = paramsArray[i]
                .split('=', 2);

            if (param.length !== 2)
                continue;

            params[param[0]] = decodeURIComponent(param[1].replace(/\+/g, " "));
        }

        return params;
    })(window.location.search.substr(1).split('&'));

    var codiad = global.codiad,
        scripts = document.getElementsByTagName('script'),
        path = scripts[scripts.length - 1].src.split('?')[0],
        curpath = path.split('/').slice(0, -1).join('/') + '/',
        instance = null;

    $(function () {
        codiad.Argon.init();
        codiad.Argon.bottomBar();
        codiad.Argon.codeExamples();
        codiad.Argon.documentation();

    });

    codiad.Argon = {

        path: curpath,

        init: function () {
            var _this = this;

            instance = this;
            this.callAction('hostname').success(function (response) {
                WebSocketArgon.init(response.data.ip);
            });

            $.get("plugins/argon/templates/editor-top-action-bar.html", function (data) {
                $('#editor-region').prepend(data);
            });

        },
        run: function () {
            return WebSocketArgon.callScript('run');
            return this.callAction('run');
        },
        update: function () {
            return WebSocketArgon.callScript('update');
        },
        pause: function () {
            return WebSocketArgon.callScript('pause');
            //return this.callAction('pause');
        },
        reset: function () {
            WebSocketArgon.callScript('reset');

            setTimeout(function () {
                WebSocketArgon.refresh();
            }, 500);
            //return this.callAction('reset');
        },
        deploy: function () {

            return false;
            return WebSocketArgon.callScript('deploy');
            return this.callAction('deploy');
        },
        compile: function () {

            return false;
            codiad.filemanager.init();
            var fullPath = '/var/www/html/workspace/' + codiad.project.getCurrent();
            return this.callScript('compile');
        },
        variables: function () {
            return this.callAction('variables');
        },
        serial: function () {
            return WebSocketArgon.callScript('serial');
        },
        callAction: function (actionName) {
            return $.get(instance.path + "controller.php?action=" + actionName, function (data) {
                return data;
            }).error(function () {
                throw Error('Request error');
            });
        },
        bottomBar: function () {
            $('#current-file').after('<div class="divider"></div><div style="float:left;display:inline-block;cursor: pointer" id="argon-output"><span>Output</span></div>');
            $('#editor-bottom-bar').prepend('<div style="overflow-y:scroll!important;display:none;height:100px;width: 100%;background: white;position: absolute;bottom: 25px;" id="argon-terminal"><div style="position:fixed;box-shadow:0px 0px 15px 0px rgba(0, 0, 0, .9);height: 10px;cursor:row-resize;background:#474747;display: block;width: 100%;" class="ui-draggable"></div><div id="argon-terminal-output" style="padding: 20px;overflow-y: scroll;!important;"></div></div>');
            $('#argon-output').on('click', function () {
                if($(this).hasClass('opened')){
                    $('#argon-terminal').slideToggle({
                        direction: "down",
                    }, 300).css('overflow-y', 'scroll');
                }else{
                    $('#argon-terminal').slideToggle({
                        direction: "up",
                    }, 300).css('overflow-y', 'scroll');
                }

                $('#argon-output').toggleClass('opened', 'closed');

            });
            $("#argon-terminal")
                .draggable({
                    axis: 'y',
                    drag: function (event, ui) {
                        newWidth = ui.position.top;
                        $("#argon-terminal").height(-(newWidth));
                        if(ui.position.top > -10){
                            return false;
                        }
                    },
                    stop: function () {
                        $(window).resize();
                        $('#editor-region')
                            .trigger('h-resize-init');
                        localStorage.setItem('codiad.sidebars.sb-left-width', $('#sb-left').width());
                    }
                });
        },
        codeExamples: function () {

            var currentProjectName;

            $(document).on('click', '.js-argon-tutorial', function () {
                var $this = $(this);
                currentProjectName = codiad.project.getCurrent();

                codiad.project.create();

                setTimeout(function () {
                    $.get("plugins/argon/templates/create-tutorial.html", function (data) {
                        $('#modal-content').html('').prepend(data);
                        $('[name=project_name]').val($this.data('title'));
                        $('.js-argon-create-tutorial').data('path', $this.data('path'));
                    });
                }, 50);

            });

            $(document).on('click', '.js-argon-create-tutorial', function () {
                var $this = $(this);
                var tutorialName = $this.data('path');
                setTimeout(function () {
                    if (currentProjectName !== codiad.project.getCurrent()) {
                        //New tutorial created
                        WebSocketArgon.callScript('tutorial ' + tutorialName + ' ' + codiad.project.getCurrent());
                    }
                }, 300);

            });

            $(document).on('click', '.js-scroll', function () {
                $('.sb-right-content').animate({
                    scrollTop: $('.js-target').offset().top - 30
                }, {
                    duration: 1500, specialEasing: {
                        width: "linear",
                        height: "easeOutBounce"
                    }
                });
            });

        },
        documentation:function () {

            $.get("plugins/argon/templates/right-side-bar.html", function (data) {
                $('.sb-right-content').html('').prepend(data);
            });

            getDocument('index.html');

            $(document).on('click','.get-doc',function () {
                getDocument($(this).data('doc-path'));
            });

            function getDocument(path) {
                $.get(instance.path + "controller.php?action=getDocumentation&path=" + path, function (response) {
                    $('.js-argon-docs > .docs').html(response.data.html);
                }).error(function () {
                    throw Error('Request error');
                })
            }

        }
    };

})(this, jQuery);