#!/bin/bash
#websocketd --port=8080 sh wsmanager.sh
#This script is a simple manager for all the other bash scripts.
#This way you only need one running websocketd process.
while read C
do
    cdir=`pwd`
    script=''
    first=''
    args=''

    # does $C have arguments?
    spcs=`echo $C | grep \  | wc -l`
    if [ $spcs -eq 0 ]; then
        first=$C
    else
        # read the first arg to determine what script to run
        IFS=' ' read -a part <<< "$C"

        first=${part[0]}
        #remove the first part of the string, leave all the rest as args to pass to script
        args=${C#${part[0]} }
    fi

    # which script to run?
    if [ "$first" == "echo" ]; then
        script='echo.sh'
    elif [ "$first" == "count" ]; then
        script='count2.sh'
    elif [ "$first" == "ping" ]; then
        script='ping.sh'
    elif [ "$first" == "update" ]; then
        script='argon_update.sh'
    elif [ "$first" == "run" ]; then
        script='argon_run.sh'
    elif [ "$first" == "reset" ]; then
        script='argon_reset.sh'
    elif [ "$first" == "pause" ]; then
        script='argon_pause.sh'
    elif [ "$first" == "pong" ]; then
        script='script.py'
    fi


    if [ ! -z $script ]; then
        #echo $cdir/$script $args
        eval $cdir/$script $args
    else
        echo "I don't understand: $C"
    fi

done
exit