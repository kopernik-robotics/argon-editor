<?php

/*
 * Copyright (c) Codiad & Andr3as, distributed
 * as-is and without warranty under the MIT License. 
 * See http://opensource.org/licenses/MIT for more information.
 * This information must remain intact.
 */
error_reporting(0);

require_once('../../common.php');
checkSession();
header("Content-type:application/json");

switch ($_GET['action']) {
    case 'getDocumentation':
        //$path = '/Applications/MAMP/htdocs/argon-scripts/docs/html/' . $_GET['path'];
        $path = '/home/pi/argon-scripts/docs/html/' . $_GET['path'];
        $html = file_get_contents($path);
        echo json_encode(['status' => 'OK', 'data' => ['html' => $html]]);
        exit();
        break;
    case 'hostname':
        exec('hostname -I', $output, $return_var);
        echo json_encode(['status' => 'OK', 'data' => ['ip' => $output]]);
        break;
    case 'run':
        $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset run" -c shutdown 2>&1';
        var_dump(exec($cmd, $output, $return_var));
        var_dump($return_var);
        var_dump($output);
        break;
    case 'pause':
        $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset run" -c shutdown 2>&1';
        var_dump(exec($cmd, $output, $return_var));
        var_dump($return_var);
        var_dump($output);
        break;
    case 'reset':
        $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset halt" -c shutdown 2>&1';
        var_dump(exec($cmd, $output, $return_var));
        var_dump($return_var);
        var_dump($output);
        break;
    case 'deploy':
        if (is_file($file_to_deploy)) {
            $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset halt" -c "flash write_image erase ' . $file_to_deploy
                . ' 0x08000000" -c shutdown 2>&1';
            var_dump(exec($cmd, $output, $return_var));
            var_dump($return_var);
            var_dump($output);
        } else {
            var_dump('ER');
        }
        break;
    case 'compile':
        if (is_file($file_to_deploy)) {
            $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset halt" -c "flash write_image erase ' . $file_to_deploy
                . ' 0x08000000" -c shutdown 2>&1';
            var_dump(exec($cmd, $output, $return_var));
            var_dump($return_var);
            var_dump($output);
        } else {
            var_dump('ER');
        }
        break;
    case 'variables':
        if (is_file($file_to_deploy)) {
            $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset halt" -c "flash write_image erase ' . $file_to_deploy
                . ' 0x08000000" -c shutdown 2>&1';
            var_dump(exec($cmd, $output, $return_var));
            var_dump($return_var);
            var_dump($output);
        } else {
            var_dump('ER');
        }
        break;
    case 'serial':
        if (is_file($file_to_deploy)) {
            $cmd = 'sudo /usr/local/bin/openocd -f /home/pi/argon-scripts/openocd/raspberry3_f446re.cfg -c init -c "reset halt" -c "flash write_image erase ' . $file_to_deploy
                . ' 0x08000000" -c shutdown 2>&1';
            var_dump(exec($cmd, $output, $return_var));
            var_dump($return_var);
            var_dump($output);
        } else {
            var_dump('ER');
        }
        break;
    case 'update':
        $cmd = 'sudo bash /var/www/html/plugins/argon/websocket/scripts/argon_update.sh';
        exec($cmd, $output, $return_var);
        echo json_encode(['status' => 'OK', 'data' => ['output' => $output]]);
        break;
    default:
        throw new Exception('Action not found');
        break;
}

function getWorkspacePath($path) {

    if (strpos($path, "/") === 0) {
        //Unix absolute path
        return $path;
    }
    if (strpos($path, ":/") !== false) {
        //Windows absolute path
        return $path;
    }
    if (strpos($path, ":\\") !== false) {
        //Windows absolute path
        return $path;
    }

    return WORKSPACE . "/" . $path;
}

?>